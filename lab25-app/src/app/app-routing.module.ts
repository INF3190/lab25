import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BonComponent } from './bon/bon.component';
import { MoyenComponent } from './moyen/moyen.component';
import { MauvaisComponent } from './mauvais/mauvais.component';


const routes: Routes = [
  { path: 'bon', component: BonComponent },
  { path: 'moyen', component: MoyenComponent },
  { path: 'mauvais', component: MauvaisComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
