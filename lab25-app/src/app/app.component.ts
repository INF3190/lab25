import { Component } from '@angular/core';
import { DonneesService } from './donnees.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Laboratoire 25 - Angular service';
  afficheliste:boolean = false;

  constructor(private donneesService: DonneesService){}

  getTous():any[]{
    return this.donneesService.getEtudiants();
  }

  afficher(){
    this.afficheliste = true;
  }

  cacher(){
    this.afficheliste = false;
  }
}
