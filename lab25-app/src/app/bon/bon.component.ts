import { Component, OnInit } from '@angular/core';
import { DonneesService } from '../donnees.service'

@Component({
  selector: 'app-bon',
  templateUrl: './bon.component.html',
  styleUrls: ['./bon.component.css']
})
export class BonComponent implements OnInit {

  columnsToDisplay = ['prenom', 'nom', 'note'];
  constructor(private donneesService: DonneesService) { }

  ngOnInit(): void {
  }
   
  getBons():any[]{
    return this.donneesService.getAvecNoteA();
  }
}
