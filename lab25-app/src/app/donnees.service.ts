import { Injectable } from '@angular/core';
import { liste }  from '../assets/data/etudiants.json';

@Injectable({
  providedIn: 'root'
})
export class DonneesService {

  private etudiants: any[]=liste;
  constructor() { }

  getEtudiants():any[]{
    return this.etudiants;
    // au besoin, remplacer resultats par la clé correspondante dan le fichier json.
  }

  getAvecNoteA():any[]{
    let returnval: any[]=[];
    let i:number = 0;
    for(let etud of this.etudiants){
       if(etud.note=='A'){
         returnval[i]=etud;
         i=i+1;
       }
    }
    return(returnval);
  }

  ngOnInit(): void {
    console.log("service marche");
    console.log(this.etudiants);
  }
}
