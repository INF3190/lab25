import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MauvaisComponent } from './mauvais.component';

describe('MauvaisComponent', () => {
  let component: MauvaisComponent;
  let fixture: ComponentFixture<MauvaisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MauvaisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MauvaisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
